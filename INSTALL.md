Leasson Quest installation with Vagrant
=======================================

Requirements
------------

Install: VirtualBox and Vagrant


Installation
------------

Execute the following commands:

To create and up the vagrant machine: $ vagrant up

To access vagrant machine: $ vagrant ssh


Run Project
-----------

$ rails s -b 0.0.0.0